<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php $slider = get_post_meta($post->ID, 'royal slider id', TRUE); ?>
  <?php if($slider) echo get_new_royalslider( (int)$slider ); ?>

  <section class="row full-w home-tile">
      <div class="center-title"><h2><?php the_title(); ?></h2><h3><?php echo get_the_date(); ?></h3></div>
  </section>

  <?php $pictures = $dynamic_featured_image->get_featured_images( get_the_ID() ); ?>
  <?php if(!empty($pictures)): ?>
  <section class="row tile location-contain">
    <div class="large-6 columns blog-thumb">
      <?php foreach($pictures as $picture): ?>
      <img src="<?php echo $picture['full']; ?>">
    <?php endforeach;?>
    </div>
    <div class="large-6 columns blog-copy">
      <?php echo the_content(); ?>
    </div>
  </section>
  <?php else: ?>
  <section class="row tile location-contain">
    <div class="full-w columns blog-copy">
      <?php echo the_content(); ?>
    </div>
  </section>
  <?php endif; ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
