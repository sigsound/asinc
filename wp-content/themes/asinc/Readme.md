# installing grunt

To compile the assets for this project on your mac all you have to do is run the command `grunt` in this directory. There are several extra steps you have to do to get started the first time; omit anything you've already installed before.

- open terminal app
- install homebrew (if you don't have this already it makes development environment setup 1,000,000 times easier on mac)
  - `ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"`
  - you might get asked to update xcode from the app store
- run `brew update`
- run `brew install node`
- run `npm install -g grunt-cli`
- in this folder(the theme) run `npm install`
- if there were no bumps in the road you can now simply type `grunt` into this directory in terminal and your assets will get compiled every time you save a file you've been working on.

#### links to more descriptive guides and info

- http://matthew-jackson.com/notes/development/setting-up-grunt-from-scratch/ (i would recommend homebrew but this shows a simpler way to get node/npm running)
- http://thechangelog.com/install-node-js-with-homebrew-on-os-x/
- http://blog.teamtreehouse.com/getting-started-with-grunt
