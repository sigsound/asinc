<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title( ' | Andrianna Shamaris', true, 'right' ); ?></title>
    <?php wp_head(); global $woocommerce;?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>?v15" />
    <script>
      var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
</head>

<body <?php body_class(); ?>>

  <nav id="ham-menu">
		<div class="closer"><a id="close-btn" href="#"><i class="fi-x"></i></a></div>
		<ul>
            <!--<li><?php get_search_form(); ?></li>-->
      <li><a href="/">Home</a></li>
      <li><a href="/catalog">Products</a></li>
      <li><a href="/collections">Collections</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/press">Press</a></li>
      <li><a href="/favorites">Favorites</a></li>
      <li><a href="/contact">Contact</a></li>
		</ul>
	</nav>

	<header class="branding">
    <ul class="navbar-social hide-for-medium-down">
      <li><a href="mailto:info@andriannashamarisinc.com"><i class="fi-mail"></i>&nbsp; info@andriannashamarisinc.com</a></li>&nbsp;|&nbsp;
      <li class="phone"><a href="tel:<?php echo get_option('soho_phone_number'); ?>"><i class="fi-telephone"></i>&nbsp; <?php echo get_option('soho_phone_number'); ?></a></li> 
    </ul>
    <a class= "hide-for-large-up" id="hamburglar"><i class="fi-list"></i></a>
    <section class="logo-contain"><h1><a href="/"><img src="<?php echo get_template_directory_uri() ?>/images/asinc_logo.png"></a></h1></section>
    <div class="row full-w border-b hide-for-medium-down top-nav">

    <nav class="hide-for-medium-down">
        
    <div class="tn-left large-4 medium-4 columns">
    <ul class="tn">
      <li><a class="<?php echo (is_front_page()) ? 'active' : ''; ?>" href="/">Home</a></li>
      <li><a class="<?php echo (is_page('about')) ? 'active' : '' ?>" href="/about">About</a></li>
      <li><a class="<?php echo (is_post_type_archive('product') || is_tax('product_tag')) ? 'active' : '' ?>" href="#" data-options="is_hover:true" data-dropdown="categories-drop">Products</a></li>
      <li><a class="<?php echo (is_page('collections') || is_tax('product_cat')) ? 'active' : '' ?>" href="/collections" data-options="is_hover:true" data-dropdown="collections-drop">Collections</a></li>
    </ul>
    <ul id="categories-drop" class="tiny f-dropdown" data-dropdown-content>
      <?php $tags = get_categories('taxonomy=product_tag&orderby=name');
            $key = get_last_uri_param();
       ?>
      <?php foreach($tags as $tag): ?>
      <li><a class="<?php if($key == $tag->slug){ echo 'active'; } ?>" href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></a></li>
      <?php endforeach; ?>
    </ul>
    <ul id="collections-drop" class="tiny f-dropdown" data-dropdown-content>
      <?php $collections = get_terms('product_cat','hide-empty=0&orderby=id');
       ?>
      <?php foreach($collections as $collection): ?>
      <li><a class="<?php if($key == $collection->slug){ echo 'active'; } ?>" href="/product-category/<?php echo $collection->slug; ?>"><?php echo $collection->name; ?></a></li>
      <?php endforeach; ?>
    </ul>
    </div>

        <!--<div class="tn-center large-4 medium-4 columns">
            <li><a href="tel:<?php echo get_option('soho_phone_number'); ?>"><?php echo get_option('soho_phone_number'); ?> <i class="fi-telephone"></i></a></li>
            |
            <li><a href="mailto:info@andriannashamarisinc.com"><i class="fi-mail"></i> info@andriannashamarisinc.com</a></li>
        </div>-->
        
				<div class="tn-right large-4 medium-4 columns">
					<ul class="tn">
						<li><a class="<?php echo (is_post_type_archive('blog')) ? 'active' : '' ?>" href="/blog">Blog</a></li>
            <li><a class="<?php echo (is_post_type_archive('press')) ? 'active' : '' ?>" href="/press">Press</a></li>
            <li><a class="<?php echo (is_page('contact')) ? 'active' : '' ?>" href="/contact">Contact</a></li>
						<li><a class="<?php echo (is_page('favorites')) ? 'active' : '' ?>" href="/favorites">&hearts;(<span id="cart-count"><?php echo count($woocommerce->cart->cart_contents); ?></span>)</a></li>
            <li>
              <form role="search" method="get" id="searchform" class="searchform" action="/">
                <div>
                  <div id="hidden-form-input">
                    <input type="text" value="" name="s" id="s">
                  </div>
                  <button type="submit" class="fi-magnifying-glass" id="searchsubmit">&nbsp;</button>
                </div>
              </form>
            </li>
					</ul>
				</div>

			</nav>

		</div>
	</header><!--branding-->

	<div class="wrapper">
