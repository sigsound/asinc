<?php get_header(); ?>
<!--<section class="row full-w collection-frame hide-for-medium-down">
  <div class="center-title"><h1 class="bigcopy-light">Collections</h1></div>
</section>-->
<section class="row collection-contain push">
<?php
$categories = get_terms('product_cat','hide-empty=0&orderby=id');

foreach($categories as $category):
  $image = wp_get_attachment_url( get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true ) );
?>

	<article class="large-6 columns">
    <h3><a href="/product-category/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></h3>
		<a href="/product-category/<?php echo $category->slug; ?>"><img class="coll-img" src="/crop.php?src=<?php echo $image; ?>&w=600&h=500&a=t&zc=2"></a>
	</article>

<?php endforeach; ?>

</section>
<?php get_footer(); ?>
