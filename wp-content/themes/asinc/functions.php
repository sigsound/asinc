<?php

//site settings

// if you want to define the searchable content
// function filter_search($query) {
//     if ($query->is_search) {
// 	    $query->set('post_type', array('press', 'blog', 'product'));
//     }
//     return $query;
// };
// add_filter('pre_get_posts', 'filter_search');
add_action('admin_print_scripts', 'asinc_admin_scripts');
add_action('admin_print_styles', 'asinc_admin_styles');

function asinc_admin_scripts() {
  wp_enqueue_script('jquery');
  wp_enqueue_script('media-upload');
  wp_enqueue_script('thickbox');
  wp_register_script('my-upload', "/wp-content/themes/asinc/js/lib/my-upload.js", array('jquery','media-upload','thickbox'));
  wp_enqueue_script('my-upload');
}

function asinc_admin_styles() {
  wp_enqueue_style('thickbox');
}

add_action('admin_init', 'site_specific_settings');
function site_specific_settings() {
  register_setting('general', 'site_contact_email', 'esc_attr');
  add_settings_field('site_contact_email', '<label for="site_contact_email">'.__('Site Contact Email Address' , 'site_contact_email' ).'</label>' , 'site_email_field' , 'general' );
  register_setting('general', 'catalog', 'esc_attr');
  add_settings_field('catalog', '<label for="catalog">'.__('Product Catalog' , 'catalog' ).'</label>', 'catalog_field', 'general' );
}
function site_email_field() {
  $value = get_option( 'site_contact_email', '' );
  echo '<input class="regular-text ltr" type="text" id="site_contact_email" name="site_contact_email" value="' . $value . '" />';
}
function catalog_field() {
  $value = get_option( 'catalog', '' );
  $html = '<p class="description">';
  $html .= 'Upload your PDF here.';
  $html .= '</p>';
  $html .= '<input id="catalog" type="text" name="catalog" value="'.$value.'" size="50">';
  $html .= '<button class="asinc-upload-button button" type="button">Upload</button>';

  echo $html;
}
add_action('admin_init', 'store_init_settings');
function store_init_settings() {
  register_setting('store_settings', 'soho_store_hours');
  register_setting('store_settings', 'soho_phone_number');
  register_setting('store_settings', 'soho_address');
  register_setting('store_settings', 'southhampton_store_hours');
  register_setting('store_settings', 'southhampton_phone_number');
  register_setting('store_settings', 'southhampton_address');
}
function store_settings_callback() {
    echo '<p>Change the copy in the store.</p>';
}
function create_store_settings_page() {
  add_menu_page('Store Info', 'Store Info', 'administrator', 'store_settings', 'build_store_settings_page');
}
add_action('admin_menu', 'create_store_settings_page');
function build_store_settings_page() {
  if (isset($_POST["update_settings"])) {
    $soho_store_hours = esc_attr($_POST["soho_store_hours"]);
    $soho_phone_number = esc_attr($_POST["soho_phone_number"]);
    $soho_address = esc_attr($_POST["soho_address"]);
    $southhampton_store_hours = esc_attr($_POST["southhampton_store_hours"]);
    $southhampton_phone_number = esc_attr($_POST["southhampton_phone_number"]);
    $southhampton_address = esc_attr($_POST["southhampton_address"]);
    update_option("soho_store_hours", $soho_store_hours);
    update_option("soho_phone_number", $soho_phone_number);
    update_option("soho_address", $soho_address);
    update_option("southhampton_store_hours", $southhampton_store_hours);
    update_option("southhampton_phone_number", $southhampton_phone_number);
    update_option("southhampton_address", $southhampton_address);
    $flash = "<div id='message' class='updated'>Settings Saved</div>";
  } else {
    $soho_store_hours = get_option("soho_store_hours");
    $soho_phone_number = get_option("soho_phone_number");
    $soho_address = get_option("soho_address");
    $southhampton_store_hours = get_option("southhampton_store_hours");
    $southhampton_phone_number = get_option("southhampton_phone_number");
    $southhampton_address = get_option("southhampton_address");
  }
  include('admin-store-settings.php');
}

//post types
function blog_post_type() {

	$labels = array(
		'name'                => _x( 'blogs', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'blog', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Blog', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Blogs', 'text_domain' ),
		'view_item'           => __( 'View Blog', 'text_domain' ),
		'add_new_item'        => __( 'Add New Blog Post', 'text_domain' ),
		'add_new'             => __( 'Add New Blog Post', 'text_domain' ),
		'edit_item'           => __( 'Edit Blog Post', 'text_domain' ),
		'update_item'         => __( 'Update Blog Post', 'text_domain' ),
		'search_items'        => __( 'Search Blog', 'text_domain' ),
		'not_found'           => __( 'No Blog Posts Found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No Blog found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'blog', 'text_domain' ),
		'description'         => __( 'blog posts', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-write-blog',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
    'rewrite'             => array('slug' => 'blog')
	);
	register_post_type( 'blog', $args );

}

// Hook into the 'init' action
add_action( 'init', 'blog_post_type', 0 );

function press_post_type() {

  $labels = array(
    'name'                => _x( 'press', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'press', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Press', 'text_domain' ),
    'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
    'all_items'           => __( 'All Press', 'text_domain' ),
    'view_item'           => __( 'View Press', 'text_domain' ),
    'add_new_item'        => __( 'Add New Press', 'text_domain' ),
    'add_new'             => __( 'Add New Press', 'text_domain' ),
    'edit_item'           => __( 'Edit Press Info', 'text_domain' ),
    'update_item'         => __( 'Update Press Info', 'text_domain' ),
    'search_items'        => __( 'Search Press', 'text_domain' ),
    'not_found'           => __( 'No Press Content Found', 'text_domain' ),
    'not_found_in_trash'  => __( 'No Press found in Trash', 'text_domain' ),
  );
  $args = array(
    'label'               => __( 'press', 'text_domain' ),
    'description'         => __( 'press content', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 6,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'menu_icon'           => 'dashicons-format-quote',
    'rewrite'             => array('slug' => 'press')
  );
  register_post_type( 'press', $args );

}

// Hook into the 'init' action
add_action( 'init', 'press_post_type', 0 );


//helpers

function asinc_add_editor_styles() {
  add_editor_style( 'style.css' );
}
add_action( 'init', 'asinc_add_editor_styles' );

register_new_royalslider_files(1);

function get_featured_collection() {
  // $first_category = wp_cache_get( 'featured_collection' );
  // if ( false === $result ) {
  	$categories_query = get_categories( array(
     'taxonomy'     => 'product_cat',
     'show_count'   => 0,
     'pad_counts'   => 0,
     'hierarchical' => 1,
     'title_li'     => '',
     'hide_empty'   => 0,
     'number'       => 1
    ) );
    $first_category = $categories_query[0];
    $first_category->image = wp_get_attachment_url( get_woocommerce_term_meta( $first_category->term_id, 'thumbnail_id', true ) );
    $first_category->products = get_posts( array( 'post_type' => 'product', 'product_cat' => $first_category->slug ) );
    foreach($first_category->products as $product) {
      $attachments = get_posts( array(
        'post_type' => 'attachment',
        'numberposts' => -1,
        'post_status' => null,
        'post__in' => explode( ',', get_post_meta( $product->ID, '_product_image_gallery', true ) )
      ) );
      $product->images = array();
      if ( $attachments ) {
        $i=0;
        foreach ( $attachments as $attachment ) {
          $product->images[$i] = wp_get_attachment_url( $attachment->ID, 'full' );
          $i++;
        }
      }
    }
  //  wp_cache_set( 'featured_collection', $first_category );
  // }
  return $first_category;
}

function get_woo_gallery_images($id) {
  $attachments = get_posts( array(
    'post_type' => 'attachment',
    'numberposts' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status' => null,
    'post__in' => explode( ',', get_post_meta( $id, '_product_image_gallery', true ) )
  ) );
  $images = array();
  if ( $attachments ) {
    $i=0;
    foreach ( $attachments as $attachment ) {
      $images[$i] = wp_get_attachment_url( $attachment->ID, 'full' );
      $i++;
    }
  }
  return $images;
}

function get_last_uri_param() {
  $url = $_SERVER['REQUEST_URI'];
  $keys = parse_url($url);
  $path = explode("/", $keys['path']);
  $end = end($path);
  if($end == '') {
    $reverse = array_reverse($path);
    return $reverse[1];
  } else {
    return $end;
  }
}

//hide menus I don't want to bother with
function remove_menus() {

  global $blog_id, $menu;

  $restricted = array(__('Posts'), __('Links'), __('Comments'));

  end ($menu);

  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
  }

}
add_action('admin_menu', 'remove_menus');


// handle price ask

function load_template_part($template, $vars=null) {
  if (is_array($vars) && !empty($vars)) {
    extract($vars);
  }
  ob_start();
  include($template);
  $var = ob_get_contents();
  ob_end_clean();
  return $var;
}


function send_price_email($email,$name,$designer,$product_id) {
  global $wpdb;

  $product = get_product( intval( $product_id ) );
  // print_r($product->get_available_variations());

  $headers  = "From: Andrianna Shamaris<INFO@ANDRIANNASHAMARISINC.COM>\r\n";
  $headers .= "Reply-To: INFO@ANDRIANNASHAMARISINC.COM\r\n";
  $headers .= "Return-Path: INFO@ANDRIANNASHAMARISINC.COM\r\n";
  $headers .= "X-Mailer: Wordpress\n";
  $headers .= 'MIME-Version: 1.0' . "\n";
  //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
  $sent_more_info = wp_mail ( $email , 'Your request for info' , load_template_part( locate_template('email-price.php', false ), array('name'=>$name, 'product'=>$product, 'designer'=>$designer, 'admin'=>false) ), $headers );
  $sent_admin_copy = wp_mail ( 'INFO@ANDRIANNASHAMARISINC.COM' , 'Request for pricing' , load_template_part( locate_template('email-price.php', false ), array('name'=>$name, 'product'=>$product, 'designer'=>$designer, 'admin'=>true, 'email'=>$email) ), $headers );
}

// add_action( 'wp_ajax_nopriv_get_price', 'asinc_ajax_nopriv_get_price' );
// add_action( 'wp_ajax_get_price', 'asinc_ajax_nopriv_get_price' );

// function get_base64_image($path) {
//   $type = pathinfo($path, PATHINFO_EXTENSION);
//   $data = file_get_contents($path);
//   echo 'data:image/' . $type . ';base64,' . base64_encode($data);
// }

add_action( 'init', 'ninja_forms_register_hooks' );
function ninja_forms_register_hooks() {
	add_action('ninja_forms_post_process', 'asinc_ninja_forms_post_process');
}

function asinc_ninja_forms_post_process(){
	global $ninja_forms_processing;

	if($ninja_forms_processing->get_form_ID() == 2) {
    $email = $ninja_forms_processing->get_field_value(6);
    $name = $ninja_forms_processing->get_field_value(7);
    $designer = $ninja_forms_processing->get_field_value(17);
    $product_id = $ninja_forms_processing->get_field_value(20);
    send_price_email($email,$name,$designer,$product_id);
  }

}