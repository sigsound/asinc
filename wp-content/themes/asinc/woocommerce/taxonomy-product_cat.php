
  <?php get_header(); ?>
    <?php $category = get_terms('product_cat','slug='.$wp_query->query_vars['product_cat']); ?>
    <?php echo do_shortcode( $category[0]->description ); ?>
    <div class="center-title"><h2><?php echo $category[0]->name; ?></h2></div>
    <div class="wrap-for-load">
      <section class="asinc-loader"><img src="/wp-content/themes/asinc/images/loader_asinc.gif" alt="loading"></section>
      <section class="catalog-contain">
        <div id="container">
          <div class="gutter-sizer"></div>
          <?php global $wp_query; ?>
          <?php if($wp_query->have_posts()) : while($wp_query->have_posts()): $wp_query->the_post(); ?>

            <?php get_template_part( 'loop', 'product' ); ?>

          <?php endwhile; endif; ?>
        
        </div>
        <nav class="asinc-pager">
          <?php if(function_exists('wp_simple_pagination')) {
          wp_simple_pagination();
          } ?> 
        </nav>
      </section>
    </div><!--.wrap-for-load->

  <?php get_footer(); ?>
