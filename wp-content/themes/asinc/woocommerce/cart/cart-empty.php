<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

?>
<section class="row catalog-contain">
  <div id="container" class="no-favorites">
    <div class="center-title"><h2>You don't have any favorites</h2></div>
    <div class="center-title"><a class="btn-light" href="/product-tag/side-tables-stools/">View Products</a></div>
  </div>
</section>
