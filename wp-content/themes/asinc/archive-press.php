<?php get_header(); ?>

  <section class="row full-w press-frame hide-for-medium-down">
    <div class="center-title"><h1 class="white">Press</h1></div>
  </section>

  <section class="row press-contain push">
    <div id="container">
      <div class="gutter-sizer"></div>

      <?php
        if(have_posts()) : while(have_posts()): the_post();
          $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
          $custom = get_post_custom();
        ?>

        <div class="item press-item">
          <?php if($image_attributes): ?>
          <a href ="<?php the_permalink(); ?>">
            <?php
              $aspect = $image_attributes[1]/$image_attributes[2];
              if($aspect >= 2) {
                $size = array(400,200); }
              elseif($aspect >= 1) {
                $size = array(400,400); }
              else {
                $size = array(400,600); }
            ?>
            <img width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>" src="/crop.php?src=<?php echo $image_attributes[0]; ?>&w=<?php echo $size[0]; ?>&h=<?php echo $size[1]; ?>&a=t&zc=1">
          </a>
          <?php endif; ?>
          <div class="press-excerpt">
            <h2 class="excerpt-title"><a href="<?php the_permalink(); ?>"><?php echo $custom["publication"][0]; ?></a></h2>
            <p class="excerpt-quote"><?php echo $custom["quote"][0]; ?></p>
            <a class="read-more" href="<?php the_permalink(); ?>"><span class="arrow">&#10141;</span>Read More</a>
          </div>
        </div>

      <?php endwhile; endif; ?>
       
    </div>
     <nav class="asinc-pager">
        <?php if(function_exists('wp_simple_pagination')) {
          wp_simple_pagination();
        } ?> 
      </nav>
  </section>

<?php get_footer(); ?>
