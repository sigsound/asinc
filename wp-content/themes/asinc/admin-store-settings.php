<div class="wrap">
  <h1>Store Information</h1>
  <?php if($flash) echo $flash; ?>
  <form method="POST" action="">
    <table class="form-table"><tbody>
      <tr valign="top"><th scope="row"><h3>Soho Store</h3><th></tr>
      <tr valign="top">
        <th scope="row">
          <label for="soho_store_hours">Soho Store Hours</label></th>
        <td>
          <textarea id="soho_store_hours" name="soho_store_hours" rows="4" cols="40"><?php echo $soho_store_hours; ?></textarea>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">
          <label for="soho_phone_number">Soho Phone Number</label></th>
        <td>
          <input type="text" id="soho_phone_number" name="soho_phone_number" value="<?php echo $soho_phone_number; ?>">
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">
          <label for="soho_address">Soho Address</label></th>
        <td>
          <textarea id="soho_address" name="soho_address" rows="4" cols="40"><?php echo $soho_address; ?></textarea>
        </td>
      </tr>
      <tr valign="top"><th scope="row"><h3>Southampton Store</h3><th></tr>
      <tr valign="top">
        <th scope="row">
          <label for="southhampton_store_hours">Southampton Store Hours</label></th>
        <td>
          <textarea id="southhampton_store_hours" name="southhampton_store_hours" rows="4" cols="40"><?php echo $southhampton_store_hours; ?></textarea>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">
          <label for="southhampton_phone_number">Southampton Phone Number</label></th>
        <td>
          <input type="text" id="southhampton_phone_number" name="southhampton_phone_number" value="<?php echo $southhampton_phone_number; ?>">
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">
          <label for="southhampton_address">Southampton Address</label></th>
        <td>
          <textarea id="southhampton_address" name="southhampton_address" rows="4" cols="40"><?php echo $southhampton_address; ?></textarea>
        </td>
      </tr>
    </tbody></table>
    <p>
      <input type="hidden" name="update_settings" value="Y" />
      <input type="submit" value="Save Changes" class="button-primary"/>
    </p>
  </form>
</div>
