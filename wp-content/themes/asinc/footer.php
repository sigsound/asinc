	<footer class="colophon">
		<div class="home_sign_up_contain">
			<div class="horiz">
				<?php echo do_shortcode( "[mc4wp_form]" ); ?>
			</div>
		</div>
		<div class="subfooter">
			<div class="row home_foot">
				<div class="large-3 columns hf-1">
					<h3>Latest Catalog</h3>
					<button onclick="window.open('<?php echo get_option('catalog'); ?>', 'download');" class="btn-white"><i class="fi-download"></i> download catalog</button>
				</div>
				<div class="large-3 columns hf-2">
					<h3>Soho</h3>
					<ul class="footbut">
						<li><a href="tel:<?php echo get_option('soho_phone_number'); ?>"><?php echo get_option('soho_phone_number'); ?></a></li>
						<?php $address = preg_split('/\n|\r/', get_option('soho_address'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($address as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
						<?php $hours = preg_split('/\n|\r/', get_option('soho_store_hours'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($hours as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>

                <div class="large-3 columns hf-4">
				<h3>What's New?</h3>
                <p>As a result of our constantly evolving collection, not everything can be represented on the website. We encourage you to contact us with your                    particular requests, and we will be happy to update you on our new arrivals.</p>
            <a href="https://www.facebook.com/AndriannaShamarisInc" target="_blank"><i class="fi-social-facebook"></i></a>
            <a href="https://twitter.com/AShamaris" target="_blank"><i class="fi-social-twitter"></i></a>
            <a href="http://www.instagram.com/andriannashamaris" target="_blank"><i class="fi-social-instagram"></i></a>
            <a href="http://www.pinterest.com/ashamaris" target="_blank"><i class="fi-social-pinterest"></i></a>
			</div>

				<div class="large-3 columns hf-3">
					<h3>Southampton</h3>
					<ul class="footbut">
						<li><a href="tel:<?php echo get_option('southhampton_phone_number'); ?>"><?php echo get_option('southhampton_phone_number'); ?></a></li>
						<?php $address = preg_split('/\n|\r/', get_option('southhampton_address'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($address as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
						<?php $hours = preg_split('/\n|\r/', get_option('southhampton_store_hours'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($hours as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>

		</div>

		<div id="viewing-request-modal" class="reveal-modal small" data-reveal>
			<h2>Contact Us</h2>
			<?php
				if( function_exists( 'ninja_forms_display_form' ) ){
					ninja_forms_display_form( 3 );
				}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>

		<div id="pricing-request-modal" class="reveal-modal small" data-reveal>
			<h2>Enter your email to receive pricing</h2>
			<?php
				if( function_exists( 'ninja_forms_display_form' ) ){
					ninja_forms_display_form( 2 );
				}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
        <div id="copyright">© 2014 Andrianna Shamaris All Rights Reserved</div>
	</footer>
</div><!--wrapper-->
<div id="fb-root"></div>
<script>

jQuery(window).load(function(){

jQuery('.catalog-contain').addClass('loaded');
jQuery('.asinc-loader').addClass('goodbye');

});

</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/bin/all.min.js?v17"></script>
<script src="//connect.facebook.net/en_US/all.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25855718-1', 'auto');
  ga('send', 'pageview');

</script>

<?php wp_footer(); ?>
</body>

</html>
