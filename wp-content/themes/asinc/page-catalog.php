<?php
	global $wp_query;
	//$page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	if ($post->post_type == 'page'){
		$wp_query = new WP_Query( array( 'post_type' => 'product', 'paged' => get_query_var( 'paged' )) );
	}
	
?>
	<?php get_header(); ?>
		<section class="row full-w catalog-frame hide-for-medium-down">
      <div class="center-title"><h1 class="white">Questions?</h1></div>
    	<div class="btn-bar-contact"><a class="btn-light" href="#contact" data-reveal-id="viewing-request-modal" data-reveal><i class="fi-mail"></i> Send us an E-mail</a></div>
    </section>
		<a id="sort-btn" class="btn-light hide-for-large-up" href="/catalog" data-options="align:bottom" data-dropdown="categories-mobile-drop">Sort by Category</a>
		<ul id="categories-mobile-drop" class="tiny f-dropdown">
			<?php $tags = get_categories('taxonomy=product_tag&orderby=name');
						$key = get_last_uri_param();
			?>
			<?php foreach($tags as $tag): ?>
			<li><a class="<?php if($key == $tag->slug){ echo 'active'; } ?>" href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></a></li>
			<?php endforeach; ?>
		</ul>
		<div class="wrap-for-load">
			<section class="asinc-loader"><img src="/wp-content/themes/asinc/images/loader_asinc.gif" alt="loading"></section>
			<section class="full-w catalog-contain">
				<div id="container">
					<div class="gutter-sizer"></div>
		      <?php if($wp_query->have_posts()) : while($wp_query->have_posts()): $wp_query->the_post(); ?>

						<?php get_template_part( 'loop', 'product' ); ?>

		      <?php endwhile;?>


			      
				</div>
				<nav class="asinc-pager">
		      		<?php if(function_exists('wp_simple_pagination')) {
	    				wp_simple_pagination();
					} ?> 
		      	</nav>
				
			</section>
		</div><!--.wrap-for-load-->
	<?php get_footer(); ?>
<?php endif; ?>
