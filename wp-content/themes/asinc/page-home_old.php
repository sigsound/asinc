<?php get_header(); ?>

<?php echo get_new_royalslider(1); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php the_content(); ?>
<?php endwhile; endif; ?>

<div class="feature-title"><p>Featured Collection</p></div>
<?php
$collection = get_featured_collection();
if($collection):
?>
</div>

<section class="row tile home-tile-4">
<div class="large-6 medium-6 columns">
<h3><a href="/product-category/<?php echo $collection->slug; ?>"><?php echo $collection->name; ?></a></h3>
<div class="slide-contain home-lifestyle-slide-contain">
<ul id="home-lifestyle-slider">
  <li><a href="/product-category/<?php echo $collection->slug; ?>"><img alt="" src="/crop.php?src=<?php echo $collection->image ?>&w=473&h=350&a=c" /></a></li>
</ul>
</div>
</div>
<div class="large-6 medium-6 hide-for-small columns">
<h3><a href="/product-category/<?php echo $collection->slug; ?>">Products</a></h3>
<div class="slide-contain home-product-slide-contain">
<ul id="home-products-slider">
  <?php foreach($collection->products as $product): ?>
  <?php if(!empty($product->images)): ?>
  <?php if(count($product->images) >= 3): ?>
    <li>
      <div class="large-4 medium-4 columns products-slide-left-col">
        <a href="/product/<?php echo $product->post_name; ?>"><img alt="" src="/crop.php?src=<?php echo $product->images[0]; ?>&w=200&h=200&a=c&zc=2" /></a>
        <a href="/product/<?php echo $product->post_name; ?>"><img alt="" src="/crop.php?src=<?php echo $product->images[1]; ?>&w=200&h=200&a=c&zc=2" /></a>
      </div>
      <div class="large-8 medium-8 columns products-slide-right-col">
        <a href="/product/<?php echo $product->post_name; ?>"><img alt="" src="/crop.php?src=<?php echo $product->images[2]; ?>&w=400&h=400&a=c&zc=2" /></a>
      </div>
    </li>
  <?php else: ?>
    <li>
      <a href="/product/<?php echo $product->post_name; ?>"><img alt="" src="/crop.php?src=<?php echo $product->images[0]; ?>&w=473&h=350&a=c&zc=2" /></a>
    </li>
  <?php endif; ?>
  <?php endif; ?>
  <?php endforeach; ?>
</ul>
<nav class="slide-nav"><span class="slide-prv" id="hp-slide-prev"></span>
<span class="slide-nxt" id="hp-slide-next"></span></nav></div>
</div>
<div class="large-6 medium-6 columns hide-for-medium-up">
<h3><a href="/product_detail.php">Products</a></h3>
<div class="slide-contain home-products-sm-slide-contain">
<ul id="home-products-sm-slider">
  <?php foreach($collection->products as $product): ?>
  <?php if(!empty($product->images)): ?>
  <?php if(count($product->images) >= 3): ?>
  <li>
    <a href="/product_detail.php"><img alt="" src="/crop.php?src=<?php echo $product->images[2]; ?>&w=473&h=350&a=c&zc=2" /></a>
  </li>
  <?php else: ?>
  <li>
    <a href="/product_detail.php"><img alt="" src="/crop.php?src=<?php echo $product->images[0]; ?>&w=473&h=350&a=c&zc=2" /></a>
  </li>
  <?php endif; ?>
  <?php endif; ?>
  <?php endforeach; ?>
</ul>
<nav class="slide-nav"><span class="slide-prv" id="hps-slide-prev"></span>
<span class="slide-nxt" id="hps-slide-next"></span></nav></div>
</div>
</section>
<?php
  endif;
?>
<div class="row full-w border-b btn-bar"><a class="btn-light" href="/collections">view more collections</a></div>

<?php get_footer(); ?>
