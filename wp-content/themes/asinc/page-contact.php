<?php get_header(); ?>
<section class="row full-w contact-frame">
  <div class="center-title"><h1 class="white">Get In Touch</h1></div>
  <div class="btn-bar-contact"><a class="btn-light" data-reveal-id="viewing-request-modal" data-reveal href="#contact"><i class="fi-mail"></i> Send us an E-mail</a></div>
</section>

<section class="row contact-copy-contain tile">
    
    <div class="large-4 columns center">
    <h1>soho</h1>
    <h2><a href="tel:<?php echo get_option('soho_phone_number'); ?>"><?php echo get_option('soho_phone_number'); ?></a></h2>
    <ul class="footbut">
      <li><h3>Store Info</h3></li>
      <?php $address = preg_split('/\n|\r/', get_option('soho_address'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($address as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
						<?php $hours = preg_split('/\n|\r/', get_option('soho_store_hours'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($hours as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
    </ul>
    <?php $address = get_option('soho_address'); ?>
    <a target="_blank" href="//maps.google.com/?q=<?php echo $address; ?>"><img src="http://maps.googleapis.com/maps/api/staticmap?&markers=size:large%7Ccolor:black%7Ccenter=<?php echo $address; ?>&zoom=17&size=661x547&sensor=false&style=saturation:-100"></a>
  </div>
  
    <div class="large-4 columns center">
        <h1>What's New?</h1>
        <p>As a result of our constantly evolving collection, not everything can be represented on the website. 
            </ br>
            We encourage you to contact us with your particular requests, and we will be happy to update you on our new arrivals.</p>
        <h1 class="soctitle"><a href="https://www.facebook.com/AndriannaShamarisInc" target="_blank"><i class="fi-social-facebook"></i></a>
        <a href="https://twitter.com/AShamaris" target="_blank"><i class="fi-social-twitter"></i></a>
        <a href="http://www.instagram.com/andriannashamaris" target="_blank"><i class="fi-social-instagram"></i></a>
        <a href="http://www.pinterest.com/ashamaris" target="_blank"><i class="fi-social-pinterest"></i></a></h1>
    </div>
  
    <div class="large-4 columns center">
    <h1>southampton</h1>
    <h2><a href="tel:<?php echo get_option('southhampton_phone_number'); ?>"><?php echo get_option('southhampton_phone_number'); ?></a></h2>
    <ul class="footbut">
      <li><h3>Store Info</h3></li>
						<?php $address = preg_split('/\n|\r/', get_option('southhampton_address'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($address as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
						<?php $hours = preg_split('/\n|\r/', get_option('southhampton_store_hours'), -1, PREG_SPLIT_NO_EMPTY); ?>
						<?php foreach($hours as $line): ?>
						<li><?php echo $line; ?></li>
						<?php endforeach; ?>
    </ul>
    <?php $address = get_option('southhampton_address'); ?>
    <a target="_blank" href="//maps.google.com/?q=<?php echo $address; ?>"><img src="http://maps.googleapis.com/maps/api/staticmap?&markers=size:large%7Ccolor:black%7Ccenter=<?php echo $address; ?>&zoom=17&size=661x547&sensor=false&style=saturation:-100"></a>
  </div>
</section>
<?php get_footer(); ?>
