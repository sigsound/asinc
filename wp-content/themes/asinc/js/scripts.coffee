shrink_nav = ->
  $branding = $('.branding')
  $(window).scroll ->
	  if $(document).scrollTop() < 170 # 128?
        $branding.removeClass 'shrinky'
        $('body').removeClass 'shrinky'
	  else
        $branding.addClass 'shrinky'
        $('body').addClass 'shrinky'

hamburger = ->
  $('#hamburglar').on 'click', (e) ->
    e.preventDefault()
    $('#ham-menu').slideDown()
    console.log 'on'

  $('#close-btn').on 'click', (e) ->
    e.preventDefault()
    $('#ham-menu').slideUp()

  $('#ham-menu a').on 'click', (e) ->
    $('#ham-menu').slideUp()

boxsliders = ->
  $home_collections_slider = $('#home-collections-slider')
  $home_products_slider = $('#home-products-slider')
  $home_products_sm_slider = $('#home-products-sm-slider')
  if $home_collections_slider.length
    $text = $('#home-collections-excerpt').fadeOut 0
    $slides = $home_collections_slider.children('li')
    $text.text $slides.first().find('img').data('caption')
    $text.fadeIn 600
    $home_collections_slider.bxSlider
      pager: false
      moveSlides: 1
      infiniteLoop: false
      nextSelector: '#hc-slide-next'
      prevSelector: '#hc-slide-prev'
      nextText: 'Onward →'
      prevText: '← Go back'
      onSlideBefore: ($elem, old_i, new_i) ->
        caption = $slides.eq(new_i).find('img').data('caption')
        if !!caption
          $text.stop(true,false).fadeOut 300, ->
            $text.text caption
            $text.fadeIn 300
  if $home_products_slider.length
    $home_products_slider.bxSlider
      pager: false
      moveSlides: 1
      infiniteLoop: false
      nextSelector: '#hp-slide-next'
      prevSelector: '#hp-slide-prev'
      nextText: 'Onward →'
      prevText: '← Go back'
  if $home_products_sm_slider.length
    $home_products_sm_slider.bxSlider
      pager: false
      moveSlides: 1
      infiniteLoop: false
      nextSelector: '#hps-slide-next'
      prevSelector: '#hps-slide-prev'
      nextText: 'Onward →'
      prevText: '← Go back'

loadMoreProducts = ($container) ->
  $('#page-loading').addClass 'loading'
  window.page += 1
  $.ajax
    type: 'GET'
    url: "#{window.paged_url}page/#{window.page}"
    success: (response) ->
      $items = $(response).find('.item')
      if $items.length
        new imagesLoaded $items, ->
          for item in $items
            $container.append(item).packery('appended', item)
    error: ->
      # make sure to make sure
      $(window).off 'scroll.infinite', throttledScroll
    complete: ->
      $('#page-loading').removeClass 'loading'

onScroll = (e) ->
  $container = $('#container')
  scroll_bottom = $(window).scrollTop() + $(window).height()
  container_bottom = $container.offset().top + $container.height()
  if scroll_bottom >= container_bottom + 150
    $(window).off 'scroll.infinite', throttledScroll
    loadMoreProducts($container)

throttledScroll = $.throttle(onScroll, 600)

onLayout = ->
  $(window).on 'scroll.infinite', throttledScroll

grids = ->
  $container = $('#container')
  if $container.length
    imagesLoaded $container[0], ->
      window.paged_url = window.location.href;
      window.page = 1;
      $container.packery
        itemSelector: '.item'
        gutter: ".gutter-sizer"
        isInitLayout: false
      unless $container.hasClass('no-ajax')
        $container.packery 'on', 'layoutComplete', $.debounce(onLayout, 1000)
      $container.packery('layout')

bind_search_form = ->
  $('form[role="search"]').on 'submit.show-form', (e) ->
    e.preventDefault()
    $(@).off 'submit.show-form'
    $(@).on 'submit.search', validate_search
    $('#hidden-form-input').css
      display: 'inline-block'
    $(@).find('input[type="text"]').focus()

validate_search = (e) ->
  if isBlank $(@).find('input[type="text"]').val()
    e.preventDefault()
    false

enable_fastclick = ->
  # slightly older android and ios have way more responsive tapping
  FastClick.attach document.body

isBlank = (str) ->
  return (!str or /^\s*$/.test(str) or str.length <= 1)

sharing = ->
  $container = $('.sharing-contain')
  if $container.length
    FB.init
      appId: '1443927249180693',
      xfbml: false
    $container.find('.fi-social-facebook').on 'click', (e) ->
      e.preventDefault()
      FB.ui
        method: 'feed'
        name: $container.data('name')
        link: location.href
        picture: $container.data('image')
        caption: 'Andrianna Shamaris Inc.'
        description: 'Leading & Inspiring Design for Over 20 Years.'
      , (response) -> false

    $container.find('.fi-social-twitter').on 'click', (e) ->
      e.preventDefault()
      tweetUrl = "//twitter.com/intent/tweet?text=" + encodeURIComponent($container.data('name').substr(0, 140)) + "&url=" + encodeURIComponent(location.href) + "&via=AShamaris"
      tweetWindow = window.open(tweetUrl, 'tweetWindow', 'height=300,width=500')
      if window.focus
        return tweetWindow.focus()

    $container.find('.fi-social-pinterest').on 'click', (e) ->
      e.preventDefault()
      pinUrl = "//pinterest.com/pin/create/button/?url=" + encodeURIComponent(location.href) + "&media=" + encodeURIComponent($container.data('image')) + "&description=" + encodeURIComponent($container.data('name'))
      pinWindow = window.open(pinUrl, 'pinWindow', 'height=400,width=600')
      if window.focus
        return pinWindow.focus()

ask_for_price = ->
  $btn = $('.ask-for-price')
  if $btn.length
    $(document).on 'click', '.ask-for-price', (e) ->
      e.preventDefault()
      product_id = $(@).data('product_id')
      $modal = $('#pricing-request-modal')
      $modal.foundation 'reveal', 'open'
#      $form = $modal.find('#pricing-form')
#      $input_email = $modal.find('#price-request-email')
#      $input_name = $modal.find('#price-request-name')
#      $form.on 'submit', (e) ->
#        e.preventDefault()
#        email = $input_email.val()
#        name = $input_name.val()
#        errors = 0
#        if isBlank(email) or not /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(email)
#          errors += 1
#          alert 'please enter an email address'
#        if isBlank(name)
#          errors += 1
#        if not errors
#          $.ajax
#            type: 'post'
#            url: ajaxurl
#            data:
#              action: 'get_price'
#              product_id: product_id
#              email: email
#              name: name
#            success: (response) ->
#              console.log response
#              if response is 'sent'
#                msg = "You will receive an email shortly"
#              else
#                msg = "There was a problem delivering your email try again later"
#              $form.html("<h3>#{msg}</h3>").siblings('h2').remove()
#              setTimeout ->
#                $('#pricing-request-modal').foundation 'reveal', 'close'
#              , 2000

ninja_popup_listener = ->
  $viewing_form = $( '#ninja_forms_form_3' )
  $viewing_form.on 'submitResponse', (e, response) ->
    console.log response
    errors = response.errors
    if errors is false
      msg = "Thank you for contacting us, we will be in touch soon."
      $viewing_form.html("<h3>#{msg}</h3>")
      $('#viewing-request-modal h2').remove()
      setTimeout ->
        $('#viewing-request-modal').foundation 'reveal', 'close'
      , 2000

  $pricing_form = $( '#ninja_forms_form_2' )
  $pricing_form.on 'submitResponse', (e, response) ->
    console.log response
    errors = response.errors
    if errors is false
     msg = "You will receive an email shortly."
     $pricing_form.html("<h3>#{msg}</h3>")
     $('#pricing-request-modal h2').remove()
     setTimeout ->
       $('#pricing-request-modal').foundation 'reveal', 'close'
     , 2000


foundation_init = ->
  Foundation.global.namespace = ''
  $(document).foundation()

favorite_btns = ->
  $btns = $('[data-favorite]')
  if $btns.length
    $btns.on 'click', (e) ->
      e.preventDefault()
      $btn = $(e.currentTarget)
      $.ajax
        type: 'post'
        url: ajaxurl
        data:
          action: 'woocommerce_add_to_cart',
          product_id: $btn.data('favorite')
          quantity: 1
        success: (response) ->
          console.log response
          unless response.error? is true
            new_count = $(response.fragments['div.widget_shopping_cart_content']).find('ul.cart_list li').length
            old_count = parseInt($('#cart-count').text());
            if new_count > old_count
              $btn.html '&hearts;added to favorites'
              $('#cart-count').text new_count
            else
              $btn.html '&hearts;already your favorite'

$(foundation_init)
$(shrink_nav)
$(hamburger)
$(boxsliders)
$(grids)
$(bind_search_form)
$(sharing)
$(ask_for_price)
$(ninja_popup_listener)
$(favorite_btns)
# $(enable_fastclick)  //apparently foundation is doing this
