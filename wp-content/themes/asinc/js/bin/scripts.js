(function() {
  var ask_for_price, bind_search_form, boxsliders, enable_fastclick, favorite_btns, foundation_init, grids, hamburger, isBlank, loadMoreProducts, ninja_popup_listener, onLayout, onScroll, sharing, shrink_nav, throttledScroll, validate_search;

  shrink_nav = function() {
    var $branding;
    $branding = $('.branding');
    return $(window).scroll(function() {
      if ($(document).scrollTop() < 170) {
        $branding.removeClass('shrinky');
        return $('body').removeClass('shrinky');
      } else {
        $branding.addClass('shrinky');
        return $('body').addClass('shrinky');
      }
    });
  };

  hamburger = function() {
    $('#hamburglar').on('click', function(e) {
      e.preventDefault();
      $('#ham-menu').slideDown();
      return console.log('on');
    });
    $('#close-btn').on('click', function(e) {
      e.preventDefault();
      return $('#ham-menu').slideUp();
    });
    return $('#ham-menu a').on('click', function(e) {
      return $('#ham-menu').slideUp();
    });
  };

  boxsliders = function() {
    var $home_collections_slider, $home_products_slider, $home_products_sm_slider, $slides, $text;
    $home_collections_slider = $('#home-collections-slider');
    $home_products_slider = $('#home-products-slider');
    $home_products_sm_slider = $('#home-products-sm-slider');
    if ($home_collections_slider.length) {
      $text = $('#home-collections-excerpt').fadeOut(0);
      $slides = $home_collections_slider.children('li');
      $text.text($slides.first().find('img').data('caption'));
      $text.fadeIn(600);
      $home_collections_slider.bxSlider({
        pager: false,
        moveSlides: 1,
        infiniteLoop: false,
        nextSelector: '#hc-slide-next',
        prevSelector: '#hc-slide-prev',
        nextText: 'Onward →',
        prevText: '← Go back',
        onSlideBefore: function($elem, old_i, new_i) {
          var caption;
          caption = $slides.eq(new_i).find('img').data('caption');
          if (!!caption) {
            return $text.stop(true, false).fadeOut(300, function() {
              $text.text(caption);
              return $text.fadeIn(300);
            });
          }
        }
      });
    }
    if ($home_products_slider.length) {
      $home_products_slider.bxSlider({
        pager: false,
        moveSlides: 1,
        infiniteLoop: false,
        nextSelector: '#hp-slide-next',
        prevSelector: '#hp-slide-prev',
        nextText: 'Onward →',
        prevText: '← Go back'
      });
    }
    if ($home_products_sm_slider.length) {
      return $home_products_sm_slider.bxSlider({
        pager: false,
        moveSlides: 1,
        infiniteLoop: false,
        nextSelector: '#hps-slide-next',
        prevSelector: '#hps-slide-prev',
        nextText: 'Onward →',
        prevText: '← Go back'
      });
    }
  };

  loadMoreProducts = function($container) {
    $('#page-loading').addClass('loading');
    window.page += 1;
    return $.ajax({
      type: 'GET',
      url: "" + window.paged_url + "page/" + window.page,
      success: function(response) {
        var $items;
        $items = $(response).find('.item');
        if ($items.length) {
          return new imagesLoaded($items, function() {
            var item, _i, _len, _results;
            _results = [];
            for (_i = 0, _len = $items.length; _i < _len; _i++) {
              item = $items[_i];
              _results.push($container.append(item).packery('appended', item));
            }
            return _results;
          });
        }
      },
      error: function() {
        return $(window).off('scroll.infinite', throttledScroll);
      },
      complete: function() {
        return $('#page-loading').removeClass('loading');
      }
    });
  };

  onScroll = function(e) {
    var $container, container_bottom, scroll_bottom;
    $container = $('#container');
    scroll_bottom = $(window).scrollTop() + $(window).height();
    container_bottom = $container.offset().top + $container.height();
    if (scroll_bottom >= container_bottom + 150) {
      $(window).off('scroll.infinite', throttledScroll);
      return loadMoreProducts($container);
    }
  };

  throttledScroll = $.throttle(onScroll, 600);

  onLayout = function() {
    return $(window).on('scroll.infinite', throttledScroll);
  };

  grids = function() {
    var $container;
    $container = $('#container');
    if ($container.length) {
      return imagesLoaded($container[0], function() {
        window.paged_url = window.location.href;
        window.page = 1;
        $container.packery({
          itemSelector: '.item',
          gutter: ".gutter-sizer",
          isInitLayout: false
        });
        if (!$container.hasClass('no-ajax')) {
          $container.packery('on', 'layoutComplete', $.debounce(onLayout, 1000));
        }
        return $container.packery('layout');
      });
    }
  };

  bind_search_form = function() {
    return $('form[role="search"]').on('submit.show-form', function(e) {
      e.preventDefault();
      $(this).off('submit.show-form');
      $(this).on('submit.search', validate_search);
      $('#hidden-form-input').css({
        display: 'inline-block'
      });
      return $(this).find('input[type="text"]').focus();
    });
  };

  validate_search = function(e) {
    if (isBlank($(this).find('input[type="text"]').val())) {
      e.preventDefault();
      return false;
    }
  };

  enable_fastclick = function() {
    return FastClick.attach(document.body);
  };

  isBlank = function(str) {
    return !str || /^\s*$/.test(str) || str.length <= 1;
  };

  sharing = function() {
    var $container;
    $container = $('.sharing-contain');
    if ($container.length) {
      FB.init({
        appId: '1443927249180693',
        xfbml: false
      });
      $container.find('.fi-social-facebook').on('click', function(e) {
        e.preventDefault();
        return FB.ui({
          method: 'feed',
          name: $container.data('name'),
          link: location.href,
          picture: $container.data('image'),
          caption: 'Andrianna Shamaris Inc.',
          description: 'Leading & Inspiring Design for Over 20 Years.'
        }, function(response) {
          return false;
        });
      });
      $container.find('.fi-social-twitter').on('click', function(e) {
        var tweetUrl, tweetWindow;
        e.preventDefault();
        tweetUrl = "//twitter.com/intent/tweet?text=" + encodeURIComponent($container.data('name').substr(0, 140)) + "&url=" + encodeURIComponent(location.href) + "&via=AShamaris";
        tweetWindow = window.open(tweetUrl, 'tweetWindow', 'height=300,width=500');
        if (window.focus) {
          return tweetWindow.focus();
        }
      });
      return $container.find('.fi-social-pinterest').on('click', function(e) {
        var pinUrl, pinWindow;
        e.preventDefault();
        pinUrl = "//pinterest.com/pin/create/button/?url=" + encodeURIComponent(location.href) + "&media=" + encodeURIComponent($container.data('image')) + "&description=" + encodeURIComponent($container.data('name'));
        pinWindow = window.open(pinUrl, 'pinWindow', 'height=400,width=600');
        if (window.focus) {
          return pinWindow.focus();
        }
      });
    }
  };

  ask_for_price = function() {
    var $btn;
    $btn = $('.ask-for-price');
    if ($btn.length) {
      return $(document).on('click', '.ask-for-price', function(e) {
        var $modal, product_id;
        e.preventDefault();
        product_id = $(this).data('product_id');
        $modal = $('#pricing-request-modal');
        return $modal.foundation('reveal', 'open');
      });
    }
  };

  ninja_popup_listener = function() {
    var $pricing_form, $viewing_form;
    $viewing_form = $('#ninja_forms_form_3');
    $viewing_form.on('submitResponse', function(e, response) {
      var errors, msg;
      console.log(response);
      errors = response.errors;
      if (errors === false) {
        msg = "Thank you for contacting us, we will be in touch soon.";
        $viewing_form.html("<h3>" + msg + "</h3>");
        $('#viewing-request-modal h2').remove();
        return setTimeout(function() {
          return $('#viewing-request-modal').foundation('reveal', 'close');
        }, 2000);
      }
    });
    $pricing_form = $('#ninja_forms_form_2');
    return $pricing_form.on('submitResponse', function(e, response) {
      var errors, msg;
      console.log(response);
      errors = response.errors;
      if (errors === false) {
        msg = "You will receive an email shortly.";
        $pricing_form.html("<h3>" + msg + "</h3>");
        $('#pricing-request-modal h2').remove();
        return setTimeout(function() {
          return $('#pricing-request-modal').foundation('reveal', 'close');
        }, 2000);
      }
    });
  };

  foundation_init = function() {
    Foundation.global.namespace = '';
    return $(document).foundation();
  };

  favorite_btns = function() {
    var $btns;
    $btns = $('[data-favorite]');
    if ($btns.length) {
      return $btns.on('click', function(e) {
        var $btn;
        e.preventDefault();
        $btn = $(e.currentTarget);
        return $.ajax({
          type: 'post',
          url: ajaxurl,
          data: {
            action: 'woocommerce_add_to_cart',
            product_id: $btn.data('favorite'),
            quantity: 1
          },
          success: function(response) {
            var new_count, old_count;
            console.log(response);
            if ((response.error != null) !== true) {
              new_count = $(response.fragments['div.widget_shopping_cart_content']).find('ul.cart_list li').length;
              old_count = parseInt($('#cart-count').text());
              if (new_count > old_count) {
                $btn.html('&hearts;added to favorites');
                return $('#cart-count').text(new_count);
              } else {
                return $btn.html('&hearts;already your favorite');
              }
            }
          }
        });
      });
    }
  };

  $(foundation_init);

  $(shrink_nav);

  $(hamburger);

  $(boxsliders);

  $(grids);

  $(bind_search_form);

  $(sharing);

  $(ask_for_price);

  $(ninja_popup_listener);

  $(favorite_btns);

}).call(this);
