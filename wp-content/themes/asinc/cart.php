<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

// wc_print_notices();

?>
<section class="row full-w favorites-frame hide-for-medium-down">
  <div class="center-title"><h1 class="bigcopy-light">Your Favorites</h1></div>
  <div class="btn-bar-contact"><a class="btn-light" href="/catalog"><i class="fi-web"></i> keep browsing</a></div>
</section>
<section class="row catalog-contain">
  <div id="container" class="no-ajax">
    <div class="gutter-sizer"></div>
    <?php
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    	$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    	$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    	if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
    		?>
        <div class="item">
          <?php $image = wp_get_attachment_url( get_post_thumbnail_id( $_product->id ) ); ?>
          <a href ="<?php echo get_permalink($_product->id); ?>"><img src="<?php echo $image; ?>"></a>
          <div class="item-info">
            <span class="add-to"><a href="<?php echo WC()->cart->get_remove_url( $cart_item_key ); ?>">Remove from Favorites</a></span>
            <span class="cat-item-title"><?php echo $_product->post->post_title; ?></span>
          </div>
        </div>
    		<?php
    	}
    } ?>
  </div>
</section>
