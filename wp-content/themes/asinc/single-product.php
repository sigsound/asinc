<?php get_header(); ?>
<div class="row full-w push">

<?php if(have_posts()) : while(have_posts()): the_post(); ?>

  <!-- IMAGE COLUMN -->
  <div class="large-7 medium-7 columns prod-det-imgs">
    <?php $images = get_woo_gallery_images(get_the_ID()); ?>
    <?php foreach($images as $image): ?>
    <img class="product-det-img" src="<?php echo $image; ?>">
    <?php endforeach; ?>
  </div>

  <div class="large-5 medium-5 columns prod-det-info">
    <h1 class="prod-title"><?php the_title(); ?></h1>
    <h4>SKU</h4>
    <p><?php echo $product->get_sku(); ?></p>
    <?php the_content(); ?>
    <?php $in_cart = false; foreach($woocommerce->cart->get_cart() as $cart_item_key => $values):
      $product = $values['data'];
      if( get_the_ID() == $product->id ) { $in_cart = true; }
      endforeach;
    ?>
    <?php if($in_cart): ?>
      <button class="coll-view btn btn-light">&hearts; your favorite</button>
    <?php else: ?>
      <button data-favorite="<?php the_ID(); ?>" class="coll-view btn btn-light">add to favorites</button>
    <?php endif; ?>

    <button class="coll-view btn btn-light ask-for-price" data-product_id="<?php the_ID(); ?>">ask for pricing</button>
    <div class="sharing-contain" data-name="<?php the_title(); ?>" data-image="<?php echo $images[0]; ?>">
      <div class="hf-4">
        <p>Share:</p>
        <a target="_blank"><i class="fi-social-facebook"></i></a>
        <a target="_blank"><i class="fi-social-twitter"></i></a>
        <a target="_blank"><i class="fi-social-pinterest"></i></a>
      </div>
    </div>
  </div>

<?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
