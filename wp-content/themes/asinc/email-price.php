<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Andrianna Shamaris</title>

<style>
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img {
	max-width: 100%;
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased;
	-webkit-text-size-adjust:none;
	width: 100%!important;
	height: 100%;
}


/* -------------------------------------
		ELEMENTS
------------------------------------- */
a { color: #99895C;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #99895C;
}

table.social {
/* 	padding:15px; */
	background-color: #ffffff;

}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ig { background-color: #915028!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn {
	display:block;
	width:100%;
}

.column tr td td {
	padding: 0;
}

/* -------------------------------------
		HEADER
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


h1.prod-title {
	border-bottom: 1px solid #99895C;
	border-top: 1px solid #99895C;
	padding: 5px 0;
	margin-bottom: 25px;
	margin-top: 50px;
}

/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap { width: 100%;}


/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;

}


/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul {
	margin-bottom: 10px;
	font-weight: normal;
	font-size:14px;
	line-height:1.6;
}
p.lead { font-size:14px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* -------------------------------------
		SIDEBAR
------------------------------------- */
ul.sidebar {
	background:#ffffff;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* ---------------------------------------------------
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block;
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap {
	padding:0!important;
	margin:0 auto;
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* -------------------------------------------
		PHONE
		For clients that support media queries.
		Nothing fancy.
-------------------------------------------- */
@media only screen and (max-width: 600px) {

	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}

	table.social div[class="column"] {
		width:auto!important;
	}

}
</style>
</head>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!--HEADER -->
<table class="head-wrap" bgcolor="#ffffff">
	<tr>
		<td></td>
		<td class="header container">

			<div class="content">
			<table bgcolor="#ffffff" class="">
				<tr>
					<td align="center">
						<img src="<?php echo get_template_directory_uri() ?>/images/andriannashamaris_logo.gif" alt="Andrianna Shamaris" />
					</td>
					<!--<td align="right"><h6 class="collapse">Sidebar Hero</h6></td>-->
				</tr>
			</table>
			</div>

		</td>
		<td></td>
	</tr>
</table>

<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<!-- content -->
			<div class="content">
				<table>
					<tr>
						<td>
							<?php if($admin): ?>
								<p><strong>New pricing request submitted.</strong></p>
								<p><?php echo $name; ?> ( <?php echo $email; ?> ) has requested more information on the <?php echo $product->post->post_title; ?>.</p>
							<?php else: ?>
								<p><strong>Hello, <?php echo $name; ?></strong></p>
								<p>Thank you for your interest in Andrianna Shamaris products. We will be following up with you shortly but in the meantime here is some information on the <?php echo $product->post->post_title; ?>.</p>
							<?php endif; ?>


							<!-- PRODUCT TITLE -->
							<h1 class="prod-title">
								<?php echo $product->post->post_title; ?>
							</h1>

							<!-- HERO PRODUCT -->
							<p><img height="300" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $product->id ) ); ?>" /></p><!-- /hero -->




						</td>
					</tr>
				</table>
			</div>

			<!-- COLUMN WRAP -->
			<div class="column-wrap">

				<div class="column">
					<table align="left">
						<tr>
							<td>
								<h3>INFORMATION</h3>
								<h4>SKU: <?php echo $product->get_sku(); ?></h4>
								<?php echo $product->post->post_content; ?>
							</td>
						</tr>
					</table>
				</div>

				<div class="column">
					<table align="left">
						<tr>
							<td>

								<ul class="sidebar">
									<?php if($product->product_type == 'simple'): ?>
									<?php $dimensions = $product->get_dimensions(); if($dimensions != ''): ?>
									<li>
										<h5>DIMENSIONS</h5>
										<p><?php $dimensions; ?></p>
									</li>
									<?php endif; ?>
									<li>
										<h5>LIST PRICE</h5>
										<?php $price = $product->get_regular_price(); ?>
										<p><?php echo ($price == '888') ? 'CALL'  :  "$".$price ?></p>
										<?php if($designer == 'yes') : ?>
										<h5>NET PRICE</h5>
										<?php $net_price = $product->get_sale_price(); ?>
										<p><?php echo ($price == '888') ? 'CALL'  :  "$".$net_price ?></p>
										<?php endif; ?>
									</li>
								<?php elseif($product->product_type == 'variable'): $variations = $product->get_available_variations(); ?>
										<li>
											<h5>DIMENSIONS</h5>
											<?php foreach($variations as $variation): ?>
											<?php if($variation['dimensions'] != ''): ?>
											<p><?php echo $variation['dimensions']." (".$variation['attributes']['attribute_color'].")"; ?></p>
											<?php endif; ?>
											<?php endforeach; ?>
										</li>
										<li>
											<h5>LIST PRICE</h5>
											<?php foreach($variations as $variation): ?>
											<p><?php echo $variation['regular_price']." (".$variation['attributes']['attribute_color'].")"; ?></p>
											<?php endforeach; ?>
											<?php if($designer == 'yes') : ?>
											<h5>NET PRICE</h5>
											<?php foreach($variations as $variation): ?>
											<p><?php echo $variation['sale_price']." (".$variation['attributes']['attribute_color'].")"; ?></p>
											<?php endforeach; ?>
											<?php endif; ?>
										</li>
										<li>
											<h5>COLORS</h5>
											<?php foreach($variations as $variation): ?>
											<p><?php echo $variation['attributes']['attribute_color']; ?></p>
											<?php endforeach; ?>
										</li>
									<?php endif; ?>
								</ul>

								<!-- social & contact -->
								<table bgcolor="#ffffff" class="social" width="100%">
									<tr>
										<td>

											<table align="left" width="100%">
										<tr>
											<td>

												<h6 class="">Connect with Us:</h6>
												<p class="">
													<a href="https://www.facebook.com/AndriannaShamarisInc" class="soc-btn fb">Facebook</a>
													<a href="https://twitter.com/AShamaris" class="soc-btn tw">Twitter</a>
													<a href="https://www.pinterest.com/ashamaris/" class="soc-btn gp">Pinterest</a>
													<a href="https://instagram.com/andriannashamaris" class="soc-btn ig">Instagram</a>
												</p>

												<h6 class="">Contact Info:</h6>
												<p>Phone: <strong>(212) 388-9898</strong><br/>
								Email: <strong><a href="emailto:info@andriannashamarisinc.com">info@andriannashamarisinc.com</a></strong></p>

											</td>
										</tr>
									</table>

										</td>
									</tr>
								</table><!-- /social & contact -->


							</td>
						</tr>
					</table>
				</div>

				<div class="clear"></div>

			</div><!-- /COLUMN WRAP -->

		</td>
		<td></td>
	</tr>
</table>

<!-- FOOTER -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">

				<!-- content -->
				<div class="content">
					<table>
						<tr>
							<td align="center">


									<a href="http://www.andriannashamarisinc.com">www.andriannashamarisinc.com</a><br>
									<p>&copy; 2014 Andrianna Shamaris Inc. All rights reserved.</p>

							</td>
						</tr>
					</table>
				</div><!-- /content -->

		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>
