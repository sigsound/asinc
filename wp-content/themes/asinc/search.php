<?php get_header(); ?>
<div class="center-title">
  <?php if(isset($wp_query->post_count) && $wp_query->post_count > 0): ?>
  <h2 class="entry-title"><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h2>
  <?php else: ?>
  <h2 class="entry-title"><?php printf( __( 'No Results Found for: %s', 'blankslate' ), get_search_query() ); ?></h2>
  <?php endif; ?>
</div>

<?php include('post-list-search.php'); ?>

<nav class="asinc-pager">
	      		<?php if(function_exists('wp_simple_pagination')) {
    				wp_simple_pagination();
				} ?> 
	      	</nav>

<?php get_footer(); ?>
