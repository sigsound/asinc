<div class="row blog-contain">

    <?php
      $i = 1;
      if(have_posts()) : while(have_posts()): the_post();
        $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
      ?>
      <?php if(isset($image_attributes[0]) && $image_attributes[0] != '' && $i%2!=0): ?>
      <article class="row tile blog-item">
        <div class="large-6 columns blog-thumb">
          <img src="/crop.php?src=<?php echo $image_attributes[0]; ?>&w=600&h=600&a=t&zc=1">
        </div>
        <div class="large-6 columns blog-copy">
          <h2 class="blog-title"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a class="btn btn-light" href="<?php the_permalink(); ?>">read more</a>
        </div>
      </article>
    <?php elseif(isset($image_attributes[0]) && $image_attributes[0] != ''): ?>
      <article class="row tile blog-item">

        <div class="large-6 columns blog-thumb">
          <img src="/crop.php?src=<?php echo $image_attributes[0]; ?>&w=600&h=600&a=t&zc=1">
        </div>

        <div class="large-6 columns blog-copy">
          <h2 class="blog-title"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a class="btn btn-light" href="<?php the_permalink(); ?>">read more</a>
        </div>

      </article>
    <?php else: ?>
      <article class="row tile blog-item">

        <div class="full-w columns blog-copy">
          <h2 class="blog-title"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a class="btn btn-light" href="<?php the_permalink(); ?>">read more</a>
        </div>

      </article>
    <?php endif; $i++; ?>
    <?php endwhile; endif; ?>

</div>
