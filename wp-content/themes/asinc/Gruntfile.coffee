module.exports = (grunt) ->
  grunt.initConfig
    pkg: '<json:package.json>'

    #watch for changes in files
    watch:
      files: ['Gruntfile.coffee', 'js/*.coffee', 'css/**/*.scss']
      tasks: 'onwatch'

    #compile coffee files
    coffee:
      lib:
        files:
          'js/bin/scripts.js': 'js/*.coffee'

    #compile sass files
    sass:
      dist:
        options:
          style: 'expanded'
        files:
          'style.css': 'css/style.scss'

    #concat javascript
    uglify:
      options:
        mangle: false
        compress: false
        beautify: true
      my_target:
        files:
          'js/bin/all.min.js': ['js/lib/jquery.debounce.js', 'js/lib/foundation.js', 'js/lib/foundation.reveal.js', 'js/lib/foundation.dropdown.js', 'js/lib/imagesloaded.pkgd.min.js', 'js/lib/jquery.bxslider.min.js', 'js/lib/packery.pkgd.min.js', 'js/lib/fastclick.js', 'js/bin/scripts.js']

  #add event to listen for file changes
  grunt.event.on "watch", (action, filepath, target) ->
    grunt.log.writeln "#{filepath} has #{action}"

  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-sass'
  grunt.loadNpmTasks 'grunt-devtools'

  grunt.registerTask 'default', ['watch']
  grunt.registerTask 'onwatch', ['coffee', 'sass', 'uglify']
